# Script for testing the distributed training using the TFRecords
import tensorflow as tf
from tensorflow.keras.applications import MobileNetV2

from nnlo.data.adapter import NumpyAdapter
from nnlo.data.dataset import DatasetGenerator
from nnlo.run import TensorflowTrainingRunner
from nnlo.train import TensorflowTrainingDriver
from nnlo.train import SingleNodeStrategy


class Cifar10DataAdapter(NumpyAdapter):
    def __init__(self, features, labels):
        scaled_features = features / 127.5 - 1.0
        encoded_labels = tf.keras.utils.to_categorical(labels, 10)

        super().__init__(scaled_features, encoded_labels)


class Cifar10DataSetGenerator(DatasetGenerator):
    def __init__(self, output_path) -> None:
        (x_train, y_train), (x_test, y_test) = tf.keras.datasets.cifar10.load_data()
        train_adapter = Cifar10DataAdapter(x_train, y_train)
        test_adapter = Cifar10DataAdapter(x_test, y_test)
        super().__init__("cifar10", train_adapter, test_adapter, output_path)


# For local running without exporter
class Cifar10InMemoryDataSetGenerator(DatasetGenerator):
    def __init__(self, batch_size) -> None:
        (x_train, y_train), (x_test, y_test) = tf.keras.datasets.cifar10.load_data()
        
        y_train = y_train / 127.5 - 1.0
        y_train = tf.keras.utils.to_categorical(y_train, 10)
        self.train_dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train)).batch(
            batch_size
        )

        y_test = y_test / 127.5 - 1.0
        y_test = tf.keras.utils.to_categorical(y_test, 10)
        self.validation_dataset = tf.data.Dataset.from_tensor_slices((x_test, y_test)).batch(
            batch_size
        )

    def generate_tf_dataset(self, num_samples, num_records=10):
        return

    def get_train_dataset(self):
        return self.train_dataset

    def get_validation_dataset(self):
        return self.validation_dataset

def model_builder():
    return MobileNetV2(input_shape=(32, 32, 3), alpha=1.0, weights=None, classes=10)

data_generator = Cifar10InMemoryDataSetGenerator(32)
driver = TensorflowTrainingDriver('nnlo_mobile_net', model_builder, data_generator,
                                  loss='categorical_crossentropy', optimizer='adam', dist_trategy=SingleNodeStrategy())
runner = TensorflowTrainingRunner(driver)
runner.run()
