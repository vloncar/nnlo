# Example training Conv net using MNIST dataset.
# It is distributed training, single-node, multiple-gpu, using NNLO lib.

import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from nnlo.run import TensorflowTrainingRunner
from nnlo.train import TensorflowTrainingDriver
from nnlo.train import SingleNodeStrategy

# Prepare the data

# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# the data, split between train and test sets
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# Scale images to the [0, 1] range
x_train = x_train.astype("float32") / 255
x_test = x_test.astype("float32") / 255
# Make sure images have shape (28, 28, 1)
x_train = np.expand_dims(x_train, -1)
x_test = np.expand_dims(x_test, -1)
print("x_train shape:", x_train.shape)
print(x_train.shape[0], "train samples")
print(x_test.shape[0], "test samples")


# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)


# Build the model

def build_model():
    model = keras.Sequential(
        [
            keras.Input(shape=input_shape),
            layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Flatten(),
            layers.Dropout(0.5),
            layers.Dense(num_classes, activation="softmax"),
        ]
    )

    return model

# Train the model

batch_size = 128
epochs = 15

# Preventing warnings:
# Consider either turning off auto-sharding or switching the auto_shard_policy to DATA to shard this dataset. 
# You can do this by creating a new `tf.data.Options()` object then setting 
# `options.experimental_distribute.auto_shard_policy = AutoShardPolicy.DATA` before applying the options 
# object to the dataset via `dataset.with_options(options)`.

train_data = tf.data.Dataset.from_tensor_slices((x_train, y_train))
val_data = tf.data.Dataset.from_tensor_slices((x_test, y_test))

train_data = train_data.batch(batch_size)
val_data = val_data.batch(batch_size)

# Disable AutoShard.
options = tf.data.Options()
options.experimental_distribute.auto_shard_policy = tf.data.experimental.AutoShardPolicy.DATA
train_data = train_data.with_options(options)
val_data = val_data.with_options(options)

driver = TensorflowTrainingDriver("nnlo_cnn_mnist", build_model, train_data,
                                  val_data, loss="categorical_crossentropy", metrics=["accuracy"],
                                  optimizer="adam", dist_trategy=SingleNodeStrategy())
runner = TensorflowTrainingRunner(driver, epochs=epochs)
model = runner.run()


# Evaluate the trained model
score = model.evaluate(val_data, verbose=0)
print("Test loss: ", score[0])
print("Test accuracy: ", score[1])
