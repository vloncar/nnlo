# Example of data-parallel training of an ENet model on the CityScapes dataset
# This example showcases the creation of a custom training driver
# Note that Cityscapes dataset requires a manual download, see:
# https://www.tensorflow.org/datasets/catalog/cityscapes

import tensorflow as tf
import tensorflow_datasets as tfds
import numpy as np

from nnlo.data.adapter import NumpyAdapter
from nnlo.data.dataset import DatasetGenerator
from nnlo.run import TensorflowTrainingRunner
from nnlo.train import TensorflowTrainingDriver
from nnlo.train import SingleNodeStrategy

classes = {7: 1, 26: 2, 24: 3}  # road  # car  # person

class_names = {0: "background", 1: "road", 2: "car", 3: "person"}
N_CLASSES = len(classes.keys()) + 1
WIDTH = 240
HEIGHT = 152
CHANNEL_AXIS = -1
CROP_FRAC = 0.05
BOX_FRAC = 0.8
BATCH_SIZE = 16


def get_preproc(seed):
    def preproc(data):
        # box_starts = [[CROP_FRAC, CROP_FRAC, 1.0 - CROP_FRAC, 1.0 - CROP_FRAC]]
        # box_widths = tf.random.uniform(shape=(1, 4), minval=-CROP_FRAC, maxval=CROP_FRAC)
        if seed is not None:
            tf.random.set_seed(seed)
        box_starts = tf.random.uniform(
            shape=(1, 2), minval=0, maxval=(1.0 - BOX_FRAC), seed=seed
        )
        boxes = tf.concat([box_starts, box_starts + [[BOX_FRAC, BOX_FRAC]]], axis=-1)
        box_idx = [0]
        # image = tf.image.resize(data["image_left"], (WIDTH, HEIGHT)) / 255.0
        # segmentation = tf.image.resize(data["segmentation_label"], (WIDTH, HEIGHT), method="nearest")
        image = (
            tf.image.crop_and_resize(
                tf.expand_dims(data["image_left"], 0),
                boxes,
                box_idx,
                crop_size=(HEIGHT, WIDTH),
                method="nearest",
            )
            / 256.0
        )
        segmentation = tf.image.crop_and_resize(
            tf.expand_dims(data["segmentation_label"], 0),
            boxes,
            box_idx,
            crop_size=(HEIGHT, WIDTH),
            method="nearest",
        )
        image = tf.squeeze(image, 0)
        segmentation = tf.squeeze(segmentation, 0)
        segmentation = tf.cast(segmentation, tf.int32)
        output_segmentation = tf.zeros(segmentation.shape, dtype=segmentation.dtype)

        for cs_class, train_class in classes.items():
            output_segmentation = tf.where(
                segmentation == cs_class, train_class, output_segmentation
            )

        # image = tf.transpose(image, [2, 0, 1])
        # segmentation = tf.transpose(segmentation, [2, 0, 1])
        return image, output_segmentation

    return preproc


def create_cityscapes_ds(split, batch_size, shuffle=True, seed=None):
    ds = tfds.load(
        "cityscapes",
        # Change this to point to the storage of TF datasets
        data_dir="/eos/user/i/iveljano/ml-dl/workspace/cern-zenuity/tensorflow_datasets",
        download=True,
        split=split,
    )
    ds = ds.map(get_preproc(seed), num_parallel_calls=tf.data.AUTOTUNE)
    if shuffle:
        ds = ds.shuffle(100)
    ds = ds.batch(batch_size)
    ds = ds.prefetch(tf.data.AUTOTUNE)
    return ds


DF = "channels_last"
CHANNEL_AXIS = -1
PAD = "valid"


def upsample_nearest(input_tensor, name, size, data_format):
    _factor = size[0]

    input_shape = input_tensor.get_shape().as_list()
    # x = tf.image.resize(input_tensor, (input_shape[1] * _factor, input_shape[2] * _factor), method="nearest")
    x = tf.keras.layers.UpSampling2D(size=size, data_format=DF)(input_tensor)
    return x

def enet_initial_block(inputs, in_depth, filters, kernel_size, pooling, activation):
    conv = tf.keras.layers.Conv2D(
        filters - in_depth,
        kernel_size=kernel_size,
        kernel_initializer="lecun_uniform",
        use_bias=False,
        data_format=DF,
        padding=PAD,
        name="conv_initial",
    )
    pool = tf.keras.layers.MaxPooling2D(
        pool_size=(pooling, pooling), strides=pooling, data_format=DF, padding=PAD
    )
    bottom_pad = 2
    right_pad = 2
    pool_pad = tf.keras.layers.ZeroPadding2D(
        padding=((0, bottom_pad), (0, right_pad)), data_format=DF
    )
    bn = tf.compat.v2.keras.layers.BatchNormalization(axis=CHANNEL_AXIS, fused=False)
    concat = tf.keras.layers.Concatenate(axis=CHANNEL_AXIS)

    pool = pool(inputs)
    x = pool_pad(pool)
    x = conv(x)
    x = bn(x)
    x = concat([x, pool])
    x = activation()(x)
    return x


def enet_bottleneck(
    inputs, downsample, upsample, filters, filters_out, activation, dropout_prob, name
):

    if downsample:
        proj_kern = 2
    else:
        proj_kern = 1

    bottom_pad = 1
    right_pad = 1
    downsample_pool = tf.keras.layers.MaxPooling2D(
        pool_size=(2, 2), strides=2, data_format=DF, padding="valid"
    )
    downsample_pad = tf.keras.layers.ZeroPadding2D(
        padding=((0, bottom_pad), (0, right_pad)), data_format=DF
    )

    projection_conv = tf.keras.layers.Conv2D(
        filters,
        kernel_size=proj_kern,
        kernel_initializer="lecun_uniform",
        use_bias=False,
        data_format=DF,
        padding=PAD,
        name=f"proj_conv_{name}",
    )
    bn1 = tf.compat.v2.keras.layers.BatchNormalization(axis=CHANNEL_AXIS, fused=False)
    bn2 = tf.compat.v2.keras.layers.BatchNormalization(axis=CHANNEL_AXIS, fused=False)
    bn3 = tf.compat.v2.keras.layers.BatchNormalization(axis=CHANNEL_AXIS, fused=False)
    bn4 = tf.compat.v2.keras.layers.BatchNormalization(axis=CHANNEL_AXIS, fused=False)

    bottom_pad = 2
    right_pad = 2
    main_pad = tf.keras.layers.ZeroPadding2D(
        padding=((0, bottom_pad), (0, right_pad)), data_format=DF
    )
    main_conv = tf.keras.layers.Conv2D(
        filters,
        kernel_size=3,
        kernel_initializer="lecun_uniform",
        use_bias=False,
        data_format=DF,
        padding=PAD,
        name=f"main_conv_{name}",
    )

    out_filter_conv = tf.keras.layers.Conv2D(
        filters_out,
        kernel_size=1,
        kernel_initializer="lecun_uniform",
        use_bias=False,
        data_format=DF,
        padding=PAD,
        name=f"out_conv_{name}",
    )

    skip_conv = tf.keras.layers.Conv2D(
        filters_out,
        kernel_size=1,
        kernel_initializer="lecun_uniform",
        use_bias=False,
        data_format=DF,
        padding=PAD,
        name=f"skip_conv_{name}",
    )

    add = tf.keras.layers.Add()

    x = inputs
    if downsample:
        x = downsample_pool(x)
    skip = x
    if downsample:
        x = downsample_pad(x)
    x = projection_conv(x)
    x = bn1(x)
    x = activation()(x)

    if upsample:
        x = upsample_nearest(x, "upsample", (2, 2), data_format=DF)

    x = main_pad(x)
    x = main_conv(x)
    x = bn2(x)
    x = activation()(x)

    x = out_filter_conv(x)
    x = bn3(x)

    # skip = main_pad(skip)
    skip = skip_conv(skip)
    skip = bn4(skip)

    if upsample:
        skip = upsample_nearest(skip, "upsample", (2, 2), data_format=DF)

    x = tf.keras.layers.SpatialDropout2D(dropout_prob, data_format=DF)(x)
    skip = tf.keras.layers.SpatialDropout2D(dropout_prob, data_format=DF)(skip)

    x = add([x, skip])
    x = activation()(x)

    return x


def enet_final_block(inputs, filters, kernel_size):
    filters = filters
    kernel_size = kernel_size
    bottom_pad = 1
    right_pad = 1
    pad = tf.keras.layers.ZeroPadding2D(
        padding=((0, bottom_pad), (0, right_pad)), data_format=DF
    )
    conv = tf.keras.layers.Conv2D(
        filters,
        kernel_size=2,
        kernel_initializer="lecun_uniform",
        use_bias=False,
        data_format=DF,
        padding=PAD,
        activation="softmax",
        name="final_block",
    )

    x = upsample_nearest(inputs, "upsample", (2, 2), data_format=DF)
    x = pad(x)
    x = conv(x)
    return x


def enet(inputs, filter_out, initial_f=32, f1=64, f2=128, f3=128, f4=64, f5=16):
    activation = tf.keras.layers.ReLU  # tf.keras.activations.relu
    x = enet_initial_block(
        inputs,
        in_depth=3,
        filters=initial_f,
        kernel_size=3,
        pooling=2,
        activation=activation,
    )

    # Bottleneck 1
    x = enet_bottleneck(
        x,
        downsample=True,
        upsample=False,
        filters=f1,
        filters_out=f1,
        activation=activation,
        dropout_prob=0.01,
        name="bottleneck_1_0",
    )
    x = enet_bottleneck(
        x,
        downsample=False,
        upsample=False,
        filters=f1,
        filters_out=f1,
        activation=activation,
        dropout_prob=0.01,
        name="bottleneck_1_1",
    )
    x = enet_bottleneck(
        x,
        downsample=False,
        upsample=False,
        filters=f1,
        filters_out=f1,
        activation=activation,
        dropout_prob=0.01,
        name="bottleneck_1_2",
    )
    # x = enet_bottleneck(x, downsample=False, upsample=False, filters=f1, filters_out=f1, activation=activation, dropout_prob=0.01)
    # x = enet_bottleneck(x, downsample=False, upsample=False, filters=f1, filters_out=f1, activation=activation, dropout_prob=0.01)

    # Bottleneck 2
    x = enet_bottleneck(
        x,
        downsample=True,
        upsample=False,
        filters=f1,
        filters_out=f2,
        activation=activation,
        dropout_prob=0.1,
        name="bottleneck_2_0",
    )
    x = enet_bottleneck(
        x,
        downsample=False,
        upsample=False,
        filters=f2,
        filters_out=f2,
        activation=activation,
        dropout_prob=0.1,
        name="bottleneck_2_1",
    )
    x = enet_bottleneck(
        x,
        downsample=False,
        upsample=False,
        filters=f2,
        filters_out=f2,
        activation=activation,
        dropout_prob=0.1,
        name="bottleneck_2_2",
    )
    # x = enet_bottleneck(x, downsample=False, upsample=False, filters=f2, filters_out=f2, activation=activation, dropout_prob=0.1)
    # x = enet_bottleneck(x, downsample=False, upsample=False, filters=f2, filters_out=f2, activation=activation, dropout_prob=0.1)

    # Bottleneck 3
    x = enet_bottleneck(
        x,
        downsample=False,
        upsample=False,
        filters=f2,
        filters_out=f3,
        activation=activation,
        dropout_prob=0.1,
        name="bottleneck_3_0",
    )
    x = enet_bottleneck(
        x,
        downsample=False,
        upsample=False,
        filters=f3,
        filters_out=f3,
        activation=activation,
        dropout_prob=0.1,
        name="bottleneck_3_1",
    )
    x = enet_bottleneck(
        x,
        downsample=False,
        upsample=False,
        filters=f3,
        filters_out=f3,
        activation=activation,
        dropout_prob=0.1,
        name="bottleneck_3_2",
    )
    # x = enet_bottleneck(x, downsample=False, upsample=False, filters=f3, filters_out=f3, activation=activation, dropout_prob=0.1)
    # x = enet_bottleneck(x, downsample=False, upsample=False, filters=f3, filters_out=f3, activation=activation, dropout_prob=0.1)

    # Bottleneck 4
    x = enet_bottleneck(
        x,
        downsample=False,
        upsample=True,
        filters=f3,
        filters_out=f4,
        activation=activation,
        dropout_prob=0.1,
        name="bottleneck_4_0",
    )
    x = enet_bottleneck(
        x,
        downsample=False,
        upsample=False,
        filters=f4,
        filters_out=f4,
        activation=activation,
        dropout_prob=0.1,
        name="bottleneck_4_1",
    )
    x = enet_bottleneck(
        x,
        downsample=False,
        upsample=False,
        filters=f4,
        filters_out=f4,
        activation=activation,
        dropout_prob=0.1,
        name="bottleneck_4_2",
    )

    # Bottleneck 5:
    x = enet_bottleneck(
        x,
        downsample=False,
        upsample=True,
        filters=f4,
        filters_out=f5,
        activation=activation,
        dropout_prob=0.1,
        name="bottleneck_5_0",
    )
    x = enet_bottleneck(
        x,
        downsample=False,
        upsample=False,
        filters=f5,
        filters_out=f5,
        activation=activation,
        dropout_prob=0.1,
        name="bottleneck_5_1",
    )

    final = enet_final_block(x, filter_out, kernel_size=2)

    return final


def create_enet(input_shape, n_classes, initial_f, f1, f2, f3, f4, f5):
    inputs = tf.keras.Input(shape=input_shape)
    outputs = enet(
        inputs,
        filter_out=n_classes,
        initial_f=initial_f,
        f1=f1,
        f2=f2,
        f3=f3,
        f4=f4,
        f5=f5,
    )
    model = tf.keras.Model(inputs, outputs)
    return model


class CityscapeGenerator(DatasetGenerator):
    def __init__(self, batch_size) -> None:
        self.train_dataset = create_cityscapes_ds("train", batch_size)
        self.validation_dataset = create_cityscapes_ds("validation", batch_size)

    def generate_tf_dataset(self, num_samples, num_records=10):
        return

    def get_train_dataset(self):
        return self.train_dataset

    def get_validation_dataset(self):
        return self.validation_dataset


def model_builder():
    model = create_enet(
                        (HEIGHT, WIDTH, 3),
                        N_CLASSES,
                        initial_f=32,
                        f1=64,
                        f2=128,
                        f3=128,
                        f4=64,
                        f5=16,
                    )
    model(np.zeros((BATCH_SIZE, HEIGHT, WIDTH, 3)))
    return model


def loss_fcn(n_classes, channel_axis):
    def loss_function(semantic_mask, out):
        target = tf.one_hot(
            tf.squeeze(semantic_mask, axis=channel_axis),
            n_classes,
            axis=channel_axis,
        )
        loss = tf.keras.losses.categorical_crossentropy(
            target, out, from_logits=False
        )
        loss = tf.reduce_mean(loss)
        return loss
    return loss_function


def callbacks():
    return [
        tf.keras.callbacks.ReduceLROnPlateau(
            monitor="val_loss",
            factor=0.2,
            patience=5,
            min_lr=0.00001,
            verbose=1,
        ),
        tf.keras.callbacks.EarlyStopping(monitor="val_loss", patience=20),
        tf.keras.callbacks.ModelCheckpoint(
            filepath="best_checkpoint.h5",
            save_weights_only=False,
            monitor="val_loss",
            mode="min",
            save_best_only=True,
            verbose=1,
        ),
        tf.keras.callbacks.CSVLogger("metrics.csv", separator=" ", append=True),
    ]
    

data_generator = CityscapeGenerator(BATCH_SIZE)
driver = TensorflowTrainingDriver('nnlo_enet_cityscapes', model_builder, data_generator,
                                  loss=loss_fcn(N_CLASSES, CHANNEL_AXIS), callbacks=callbacks(), 
                                  optimizer='adam', dist_trategy=SingleNodeStrategy())
runner = TensorflowTrainingRunner(driver)
runner.run()
