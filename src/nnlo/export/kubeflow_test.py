# Just for testing kubeflow and pipelines


import os
from datetime import datetime, timedelta

import kfp
import kfp.components as comp
import yaml
from kubernetes import client as k8s_client
from utils import cern_sso

sso_cookie = None

cookie_file = "./cookies.txt"


def get_cern_cookie():
    # First check if cookie already exists
    if os.path.isfile(cookie_file):
        with open(cookie_file, "r") as file:
            keycloak_sessions = [
                line.split("\t")
                for line in file.readlines()
                if "KEYCLOAK_SESSION" in line
            ]
            # Ensure cookie has at least 10 minutes of validity left.
            current_ts = datetime.now() - timedelta(minutes=10)
            # Netscape format always adds timestamp as the 5th index: https://unix.stackexchange.com/a/210282/359160
            expire_ts = datetime.utcfromtimestamp(int(keycloak_sessions[0][4]))
            if expire_ts < current_ts:
                cern_sso.save_sso_cookie(
                    "https://ml.cern.ch", cookie_file, False, "auth.cern.ch"
                )
    else:
        cern_sso.save_sso_cookie(
            "https://ml.cern.ch", cookie_file, False, "auth.cern.ch"
        )


get_cern_cookie()


def get_authservice_cookie(cookie_path):
    cookie_jar = cern_sso.load_cookies_lwp(cookie_path)

    auth_srv_cookie_value = list(cookie_jar)[-1].value
    return f"authservice_session={auth_srv_cookie_value}"


client = kfp.Client(
    host="https://ml.cern.ch/pipeline", cookies=get_authservice_cookie(cookie_file)
)


# EOS stuff
krb_secret = k8s_client.V1SecretVolumeSource(secret_name="krb-secret")
krb_secret_volume = k8s_client.V1Volume(name="krb-secret-vol", secret=krb_secret)
krb_secret_volume_mount = k8s_client.V1VolumeMount(
    name=krb_secret_volume.name, mount_path="/secret/krb-secret-vol"
)

tmp_host_path = k8s_client.V1HostPathVolumeSource(path="/tmp")
tmp_volume = k8s_client.V1Volume(name="tmp", host_path=tmp_host_path)
tmp_volume_mount = k8s_client.V1VolumeMount(
    name=tmp_volume.name, mount_path="/tmp-host"
)

eos_host_path = k8s_client.V1HostPathVolumeSource(path="/var/eos")
eos_volume = k8s_client.V1Volume(name="eos", host_path=eos_host_path)
eos_volume_mount = k8s_client.V1VolumeMount(name=eos_volume.name, mount_path="/eos")


def eos_post_process(pipeline_file, outfile):
    with open(pipeline_file, "r") as stream:
        pip_dict = yaml.safe_load(stream)

    copy_command = "cp /secret/krb-secret-vol/krb5cc_1000 /tmp/krb5cc_1000"
    chmod_command = "chmod 600 /tmp/krb5cc_1000"
    pip_command = "pip install git+https://gitlab.cern.ch/vloncar/nnlo.git#egg=nnlo"

    for template in pip_dict["spec"]["templates"]:
        if "container" in template.keys():
            component_command_list = template["container"]["command"][2].split("\n")
            component_command_list.insert(2, copy_command)
            component_command_list.insert(3, chmod_command)
            component_command_list.insert(4, pip_command)

            joined_string = "\n".join(component_command_list)

            template["container"]["command"][2] = joined_string

    with open(outfile, "w") as outfile:
        yaml.dump(pip_dict, outfile, default_flow_style=False)


pipeline_name = "hello-pipeline-world"
pipeline_file = pipeline_name + ".yaml"
experiment_name = pipeline_name


def first_function(output_file: comp.OutputPath("File")):
    with open(output_file, "w") as file:
        file.write("Hello world!")


def second_function(input_file: comp.InputPath(), output_file: comp.OutputPath("File")):
    with open(input_file, "r") as file:
        message = file.read()

    with open(output_file, "w") as file:
        file.write("I have read: " + message)


def eos_function(output_file: comp.OutputPath("File")):
    import os

    output = ""
    output += os.popen("pwd").read() + "\n"
    output += os.popen("ls -la").read() + "\n"
    output += os.popen("ls -la /").read() + "\n"
    output += os.popen("whoami").read() + "\n"
    # output += os.popen('ls -la /secret/krb-secret-vol').read() + '\n'
    output += os.popen("ls -la /eos/user/i/iveljano").read()

    with open(output_file, "a") as file:
        file.write("Output of eos test: " + output)


first_function_comp = comp.func_to_container_op(
    func=first_function,
    base_image="gitlab-registry.cern.ch/ai-ml/kubeflow_images/tensorflow-notebook-gpu-2.1.0:v0.6.1-33",
)

second_function_comp = comp.func_to_container_op(
    func=second_function,
    base_image="gitlab-registry.cern.ch/ai-ml/kubeflow_images/tensorflow-notebook-gpu-2.1.0:v0.6.1-33",
)

eos_function_comp = comp.func_to_container_op(
    func=eos_function,
    base_image="gitlab-registry.cern.ch/ai-ml/kubeflow_images/tensorflow-notebook-gpu-2.1.0:v0.6.1-33",
)


@kfp.dsl.pipeline(name="ML 00", description="ML 00")
def ml_pipeline_first():
    # file_path = first_function_comp()
    # second_function_comp(file_path.output)
    eos_function_comp().add_volume(krb_secret_volume).add_volume_mount(
        krb_secret_volume_mount
    ).add_volume(eos_volume).add_volume_mount(eos_volume_mount)


pipeline_id = client.get_pipeline_id(name=pipeline_name)
if pipeline_id:
    client.delete_pipeline(pipeline_id)

workflow = kfp.compiler.Compiler().compile(ml_pipeline_first, pipeline_file)
eos_post_process(pipeline_file, pipeline_file)

client.upload_pipeline(pipeline_file, pipeline_name)
exp = client.create_experiment(name=experiment_name, namespace="irena-veljanovic")
run = client.run_pipeline(exp.id, pipeline_name, pipeline_file)

print("*** run submitted ***")
