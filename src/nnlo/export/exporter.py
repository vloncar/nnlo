from abc import ABC, abstractmethod

import tensorflow as tf

class Exporter(ABC):
    @abstractmethod
    def export(self, driver, **kwargs):
        raise NotImplementedError

