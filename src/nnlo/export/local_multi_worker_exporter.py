import inspect
import os
import sys

from .exporter import Exporter
from nnlo.train import MultiNodeStrategy

def _write_config(config_filename, dist_strategy):
    # Writing the cluster config into json file

    output_file = open(f'{config_filename}','w')
    output_file.write(dist_strategy.tf_config)
    output_file.close()

def _write_runner(template_file, output_file_name, driver_module, driver_dir, driver_class, dist_strategy, config_filename):
    # Writing Python script for running multi worker training
    
    template = open(template_file,'r')
    output_file = open(f'{output_file_name}','w')

    indent = '    '

    for line in template.readlines():

        if '# nnlo-insert import' in line:
            newline = f'sys.path.append("{driver_dir}")\n'
            newline += f'from {driver_module} import {driver_class}\n'
        elif '# nnlo-insert config-filename' in line:
            newline = f'CONFIG_FILENAME = "{config_filename}"\n'
        elif '# nnlo-insert visible-devices' in line:
            if dist_strategy.visible_devices is not None:
                newline = indent + f'os.environ["CUDA_VISIBLE_DEVICES"] = "{dist_strategy.visible_devices_env}"\n'
            else:
                newline = ''
        elif '# nnlo-insert driver' in line:
            newline = indent + f'driver = {driver_class}()\n'
        else:
            newline = line
        output_file.write(newline)

    template.close()
    output_file.close()


class MultiWorkerExporter(Exporter):
    def export(self, driver, output_file_name, config_filename='nnlo_worker_config_proba.json'):

        file = inspect.getfile(driver.__class__)
        driver_module = os.path.splitext(os.path.basename(file))[0]
        driver_dir = os.path.dirname(file)
        driver_class = driver.__class__.__name__

        dist_strategy = driver.get_dist_strategy()
        assert(isinstance(dist_strategy, MultiNodeStrategy))

        if not output_file_name.endswith('.py'):
            output_file_name += '.py'
        if not config_filename.endswith('.json'):
            config_filename += '.json'

        _write_config(config_filename, dist_strategy)

        filedir = os.path.dirname(os.path.abspath(__file__))
        template_file = os.path.join(filedir, 'templates/multi_worker_runner.py')
        _write_runner(template_file, output_file_name, driver_module, driver_dir, driver_class, dist_strategy, config_filename)
