import inspect
import json
import os
import pathlib
import tempfile
from importlib import util as import_util

import tensorflow as tf

from nnlo.export.exporter import Exporter
from nnlo.run.runner import Runner


def is_keras_model(model):
    """Checking if the loss, optimizer, layers, metrics, callbacks, model are not custom."""
    pass


def is_keras_object(obj, keras_module):
    if isinstance(obj, str):
        return True  # We assume that string is convertable by the `get` function
    elif keras_module in obj.__module__:
        return True
    else:
        return False


class LocalExporter(Exporter):
    def export(
        self,
        driver,
        output_file_name="local_runner.py",
    ):
        if is_keras_object(driver.get_model()):
            pass
        # return GenericLocalExporter(driver, output_file_name).export()
        else:
            return CustomLocalExporter(driver, output_file_name).export()


class CustomLocalExporter(Exporter):
    def _write_runner(self, template_file, output_file_name, driver):
        # Writing Python script for running training on local machine

        if inspect.isclass(driver):
            driver_class = driver
        else:
            driver_class = driver.__class__
        driver_cls_file = inspect.getfile(driver_class)
        driver_path = os.path.abspath(driver_cls_file)

        template = open(template_file, "r")
        output_file = open(f"{output_file_name}", "w")

        output_path = os.path.abspath(output_file.name)
        project_path = os.path.dirname(output_path) + "/" + driver.get_project_name()

        # Save the model in SavedModel format
        model = driver.get_model()
        model.save(project_path, save_format="tf")

        indent = "    "
        indent2 = indent + indent

        for line in template.readlines():

            if "# nnlo-insert import" in line:
                newline = (
                    indent
                    + f'spec = import_util.spec_from_file_location("tmp", "{driver_path}")\n'
                )

            elif "# nnlo-insert strategy" in line:
                newline = indent + f"driver = module.{driver_class.__name__}()\n"
                # newline = indent + f"strategy = {driver.get_dist_strategy().to_tf_strategy()}\n"
                newline = (
                    indent + f"strategy = tf.distribute.MirroredStrategy()\n"
                )  # TODO Promeni da cita iz drivera

            elif "# nnlo-insert loss" in line:
                loss = driver.get_loss()
                if is_keras_object(loss, "keras.losses"):
                    # Keras loss
                    loss = tf.keras.losses.get(loss)
                    loss_cfg = json.dumps(loss.get_config())
                    loss_class = loss.__class__.__name__
                    with open(project_path + "/loss_cfg.json", "w") as loss_cfg_file:
                        loss_cfg_file.write(loss_cfg)

                    json_load_str = f"with open('{project_path}/loss_cfg.json') as f: loss_cfg = json.loads(f.read())"
                    loss_str = (
                        f"loss_fn = tf.keras.losses.{loss_class}.from_config(loss_cfg)"
                    )

                    newline = indent + json_load_str + "\n"
                    newline += indent + loss_str + "\n"
                else:
                    # Custom loss
                    loss_source = inspect.getsource(loss)
                    print(f"Driver loss: {driver._loss}")
                    print(f"Driver get loss: {driver.get_loss()}")
                    loss_str = "".join(
                        [indent + line + "\n" for line in loss_source.split("\n")]
                    )
                    loss_name = driver._loss.__name__
                    loss_str += indent + f"loss_fn = {loss_name}\n"

                    newline = loss_str

            elif "# nnlo-insert optimizer" in line:
                opt = driver.get_optimizer()
                if is_keras_object(opt, "keras.optimizer"):
                    # Obtain the Keras optimizer instance and serialize it
                    opt = tf.keras.optimizers.get(opt)
                    opt_cfg = json.dumps(opt.get_config())
                    opt_class = opt.__class__.__name__
                    with open(
                        project_path + "/optimizer_cfg.json", "w"
                    ) as opt_cfg_file:
                        opt_cfg_file.write(opt_cfg)

                    json_load_str = f"with open('{project_path}/optimizer_cfg.json') as f: optimizer_cfg = json.loads(f.read())"
                    optimizer_str = f"optimizer = tf.keras.optimizers.{opt_class}.from_config(optimizer_cfg)"

                    newline = indent2 + json_load_str + "\n"
                    newline += indent2 + optimizer_str + "\n"
                else:
                    raise Exception("Custom optimizers are not supported yet.")

            elif "# nnlo-insert load-model" in line:
                newline = indent2 + f"model = tf.saved_model.load('{project_path}')\n"

            elif "# nnlo-insert epoch" in line:
                newline = indent2 + "epochs = " + str(driver._epochs) + "\n"

            elif "# nnlo-insert batch-size" in line:
                newline = indent2 + "batch_size = " + str(driver._batch_size) + "\n"

            elif "# nnlo-insert callbacks" in line:
                newline = indent2 + "callbacks = " + str(driver._callbacks) + "\n"

            else:
                newline = line

            output_file.write(newline)

        template.close()
        output_file.close()

        return output_path

    def export(
        self,
        driver,
        output_file_name="local_runner.py",
    ):
        if not output_file_name.endswith(".py"):
            output_file_name += ".py"

        filedir = os.path.dirname(os.path.abspath(__file__))
        template_file = os.path.join(filedir, "templates/local_runner.py")
        module_path = self._write_runner(template_file, output_file_name, driver)

        # Save model so that the runner can load it
        driver.get_model().save(driver.get_project_name() + "/initial_model")

        return _LocalRunner(
            module_path,
            model_save_path=driver.get_project_name() + "/final_model",
            metrics_path=driver.get_project_name() + "/final_model_metrics",
        )


class _LocalRunner(Runner):
    def __init__(self, module_path, model_save_path, metrics_path):
        super().__init__(None)
        self.module_path = module_path
        self.model_save_path = model_save_path
        self.metrics_path = metrics_path

    def run(self):
        spec = import_util.spec_from_file_location("tmp", self.module_path)
        module = import_util.module_from_spec(spec)
        spec.loader.exec_module(module)

        # Run the job
        module.run(self.model_save_path, self.metrics_path)
