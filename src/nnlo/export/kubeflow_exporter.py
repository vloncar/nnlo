import inspect
import os
import pathlib
import tempfile
from datetime import datetime, timedelta
from importlib import util as import_util

import kfp
import kfp.components as comp
import yaml
from kubernetes import client as k8s_client

from nnlo.export.exporter import Exporter
from nnlo.export.utils import cern_sso
from nnlo.run.runner import Runner

sso_cookie = None

cookie_path = os.path.join(os.getcwd(), ".sso_cookie")
pathlib.Path(cookie_path).mkdir(parents=True, exist_ok=True)
cookie_file = os.path.join(cookie_path, "cookies.txt")


def get_cern_cookie():
    # First check if cookie already exists
    if os.path.isfile(cookie_file):
        with open(cookie_file, "r") as file:
            keycloak_sessions = [
                line.split("\t")
                for line in file.readlines()
                if "KEYCLOAK_SESSION" in line
            ]
            # Ensure cookie has at least 10 minutes of validity left.
            current_ts = datetime.now() - timedelta(minutes=10)
            # Netscape format always adds timestamp as the 5th index: https://unix.stackexchange.com/a/210282/359160
            expire_ts = datetime.utcfromtimestamp(int(keycloak_sessions[0][4]))
            # Check if the cookie has expired and if yes generate a new one
            if expire_ts < current_ts:
                cern_sso.save_sso_cookie(
                    "https://ml.cern.ch", cookie_file, False, "auth.cern.ch"
                )
    else:
        cern_sso.save_sso_cookie(
            "https://ml.cern.ch", cookie_file, False, "auth.cern.ch"
        )


def get_authservice_cookie(cookie_path):
    cookie_jar = cern_sso.load_cookies_lwp(cookie_path)

    auth_srv_cookie_value = list(cookie_jar)[-1].value
    return f"authservice_session={auth_srv_cookie_value}"


class KubeflowExporter(Exporter):
    def _write_runner(
        self, template_file, output_file_name, driver
    ):
        # Writing Python script for running training on kubeflow

        if inspect.isclass(driver):
            driver_class = driver
        else:
            driver_class = driver.__class__
        driver_cls_file = inspect.getfile(driver_class)
        driver_module = os.path.splitext(os.path.basename(driver_cls_file))[0]
        driver_path = os.path.abspath(driver_cls_file)

        if driver_path.startswith("/eos/home"):
            driver_path = driver_path.replace(
                "/eos/home-", "/eos/user/"
            )  # We do this because ML service doesn't have access to /eos/home-* but only to /eos/user

        template = open(template_file, "r")
        output_file = open(f"{output_file_name}", "w")

        output_path = os.path.abspath(output_file.name)

        indent = "    "

        for line in template.readlines():

            if "# nnlo-insert import" in line:
                newline = (
                    indent
                    + f'spec = import_util.spec_from_file_location("tmp", "{driver_path}")\n'
                )
            elif "# nnlo-insert driver" in line:
                newline = indent + f"driver = module.{driver_class.__name__}()\n"
            elif "# nnlo-insert loss" in line:
                if True: #TODO Irena da dovrsi: detektovati da li se koristi custom loss
                    loss_source = inspect.getsource(driver._loss)
                    loss_str = "".join([indent + line + "\n" for line in loss_source.split("\n")])
                    loss_name = driver._loss.__name__
                    loss_str += indent + f"loss_fn = {loss_name}\n"
                else:
                    loss_str = indent + "loss_fn = driver.driver._loss\n"
                newline = loss_str
            else:
                newline = line
            output_file.write(newline)

        template.close()
        output_file.close()

        return output_path

    def export(
        self,
        driver,
        kubflow_host,
        kubeflow_client_params={},
        base_image=None,
        namespace=None,
        pipeline_name=None,
        experiment_name=None,
        output_file_name="kubeflow_runner.py",
    ):
        client = kfp.Client(host=kubflow_host, **kubeflow_client_params)
        if not pipeline_name:
            pipeline_name = "nnlo-pipeline"
        if not experiment_name:
            experiment_name = "nnlo-experiment"

        pipeline_tmpfile = tempfile.NamedTemporaryFile(
            prefix=pipeline_name, suffix=".yaml", delete=False
        )
        pipeline_file = pipeline_tmpfile.name

        if not output_file_name.endswith(".py"):
            output_file_name += ".py"

        filedir = os.path.dirname(os.path.abspath(__file__))
        template_file = os.path.join(filedir, "templates/kubeflow_runner.py")
        train_script_path = self._write_runner(
            template_file, output_file_name, driver
        )

        # EOS stuff
        krb_secret = k8s_client.V1SecretVolumeSource(secret_name="krb-secret")
        krb_secret_volume = k8s_client.V1Volume(
            name="krb-secret-vol", secret=krb_secret
        )
        krb_secret_volume_mount = k8s_client.V1VolumeMount(
            name=krb_secret_volume.name, mount_path="/secret/krb-secret-vol"
        )

        eos_host_path = k8s_client.V1HostPathVolumeSource(path="/var/eos")
        eos_volume = k8s_client.V1Volume(name="eos", host_path=eos_host_path)
        eos_volume_mount = k8s_client.V1VolumeMount(
            name=eos_volume.name, mount_path="/eos"
        )

        def eos_post_process(pipeline_file, outfile):
            with open(pipeline_file, "r") as stream:
                pip_dict = yaml.safe_load(stream)

            copy_command = "cp /secret/krb-secret-vol/krb5cc_1000 /tmp/krb5cc_1000"
            chmod_command = "chmod 600 /tmp/krb5cc_1000"

            for template in pip_dict["spec"]["templates"]:
                if "container" in template.keys():
                    component_command_list = template["container"]["command"][2].split(
                        "\n"
                    )
                    component_command_list.insert(2, copy_command)
                    component_command_list.insert(3, chmod_command)

                    joined_string = "\n".join(component_command_list)

                    template["container"]["command"][2] = joined_string

            with open(outfile, "w") as outfile:
                yaml.dump(pip_dict, outfile, default_flow_style=False)

        spec = import_util.spec_from_file_location("tmp", output_file_name)
        module = import_util.module_from_spec(spec)
        spec.loader.exec_module(module)

        def train_wrapper(
            train_script: str,
            metrics_path: comp.OutputPath("Metrics"),
            trained_model: comp.OutputPath("Model"),
        ):
            import os
            import sys
            from importlib import util as import_util

            if train_script.startswith("/eos/home"):
                train_script = train_script.replace("/eos/home-", "/eos/user/")

            os.chdir(os.path.dirname(train_script))

            # TODO Irena will remove this once she cleans up her branch :D
            sys.path.append("/eos/user/i/iveljano/ml-dl/workspace/nnlo/src")

            spec = import_util.spec_from_file_location("tmp", train_script)
            module = import_util.module_from_spec(spec)
            spec.loader.exec_module(module)

            module.train_function(metrics_path, trained_model)

        train_function_comp = comp.func_to_container_op(
            func=train_wrapper, base_image=base_image
        )

        @kfp.dsl.pipeline(name="ML 00", description="ML 00")
        def train_pipeline(train_script: str = train_script_path):
            train_function_comp(train_script).add_volume(
                krb_secret_volume
            ).add_volume_mount(krb_secret_volume_mount).add_volume(
                eos_volume
            ).add_volume_mount(
                eos_volume_mount
            )

        # kfp compiler
        pipeline_id = client.get_pipeline_id(name=pipeline_name)
        if pipeline_id:
            client.delete_pipeline(pipeline_id)
        workflow = kfp.compiler.Compiler().compile(train_pipeline, pipeline_file)
        eos_post_process(pipeline_file, pipeline_file)
        client.upload_pipeline(pipeline_file, pipeline_name)
        exp = client.create_experiment(name=experiment_name, namespace=namespace)

        return _KubeflowRunner(client, exp.id, pipeline_name, pipeline_file)


class CERNKubeflowExporter(KubeflowExporter):
    def export(
        self,
        driver,
        namespace,
        cookie_path=cookie_file,
        pipeline_name=None,
        experiment_name=None,
    ):
        get_cern_cookie()
        client_params = {"cookies": get_authservice_cookie(cookie_path)}
        return super().export(
            driver,
            "https://ml.cern.ch/pipeline",
            kubeflow_client_params=client_params,
            namespace=namespace,
            pipeline_name=pipeline_name,
            experiment_name=experiment_name,
            base_image="iveljano/nnlo:1",
        )


class _KubeflowRunner(Runner):
    def __init__(self, client, exp_id, pipeline_name, pipeline_file):
        super().__init__(None)
        self.client = client
        self.exp_id = exp_id
        self.pipeline_name = pipeline_name
        self.pipeline_file = pipeline_file

    def run(self):
        self.client.run_pipeline(self.exp_id, self.pipeline_name, self.pipeline_file)
