import json
import os
from importlib import util as import_util

import tensorflow as tf


def _parse_function(example_proto):
    features = {
        "feature": tf.io.FixedLenFeature([], tf.string),
        "label": tf.io.FixedLenFeature([], tf.string),
    }

    parsed_features = tf.io.parse_single_example(example_proto, features)
    feature = parsed_features["feature"]
    label = parsed_features["label"]

    feature = tf.io.parse_tensor(feature, out_type=tf.float32)
    label = tf.io.parse_tensor(label, out_type=tf.float32)

    feature = tf.reshape(feature, (100, 100, 1))
    return feature, label


def prepare_dataset(dataset, batch_size, shuffle=False):
    AUTOTUNE = tf.data.AUTOTUNE
    if shuffle:
        dataset = dataset.shuffle(buffer_size=256)
    dataset = dataset.prefetch(buffer_size=AUTOTUNE)
    dataset = dataset.batch(batch_size)
    return dataset


def load_tfrecord(tfrecord_path, batch_size=1):
    def decode_example(example_proto):
        dataset = tf.data.TFRecordDataset(tfrecord_path)  # load tfrecord file
        dataset = dataset.map(_parse_function)  # parse data into tensor
        dataset = prepare_dataset(dataset, batch_size, shuffle=False)

        return dataset

    dataset = decode_example(tfrecord_path)
    return dataset


def run(
    trained_model_path,
    trained_model_metrics_path,
):

    # nnlo-insert import
    module = import_util.module_from_spec(spec)
    spec.loader.exec_module(module)

    # nnlo-insert strategy

    # nnlo-insert loss

    # nnlo-insert batch-size

    # nnlo-insert optimizer

    # nnlo-insert epoch

    # nnlo-insert callbacks

    # nnlo-insert dataset filenames

    train_data = load_tfrecord(train_filenames, batch_size)
    validation_data = load_tfrecord(valid_filenames, batch_size)

    with strategy.scope():
        # nnlo-insert load-model

        model.compile(
            optimizer=optimizer,
            loss=loss_fn,
        )

        model.fit(
            train_data,
            epochs=epochs,
            validation_data=validation_data,
        )

    model.save(trained_model_path)
