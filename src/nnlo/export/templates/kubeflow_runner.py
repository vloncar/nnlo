from typing import List

import kfp.components as comp


def train_function(
    metrics_file_path: comp.OutputPath("Metrics"),
    trained_model_path: comp.OutputPath("Model"),
):
    from importlib import util as import_util

    import tensorflow as tf

    import nnlo

    class NNLOMetrics(tf.keras.callbacks.Callback):
        def __init__(self, metrics_file, metrics_list: List):
            self.metrics_file = metrics_file
            self.metrics_list = metrics_list

        def on_epoch_end(self, epoch, logs=None):
            if not logs:
                return
            with open(self.metrics_file, "a") as f:
                metrics_string = str(epoch) + " "
                metrics_string += " ".join(
                    [str(logs[metric]) for metric in self.metrics_list]
                )
                f.write(metrics_string)

    # nnlo-insert import
    module = import_util.module_from_spec(spec)
    spec.loader.exec_module(module)

    # nnlo-insert driver

    # nnlo-insert loss

    with driver._dist_strategy.to_tf_strategy().scope():
        model = driver.get_model()
        model.compile(
            optimizer=driver._optimizer,
            loss=loss_fn,
        )

    model.fit(
        driver.train_path,
        epochs=driver._epochs,
        validation_data=driver.valid_path,
        # callbacks=[NNLOMetrics(metrics_file_path, ["loss", "val_loss", "sparse_categorical_accuracy", "val_sparse_categorical_accuracy"])],
        callbacks=[
            tf.keras.callbacks.CSVLogger(metrics_file_path, separator=" ", append=True)
        ],
    )

    model.save(trained_model_path)
