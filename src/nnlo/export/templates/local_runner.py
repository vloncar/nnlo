import json
import os
from importlib import util as import_util

import tensorflow as tf

# os.environ['CUDA_VISIBLE_DEVICES']='1' #TODO Delete this


def _parse_function(example_proto):
    features = {
        "feature": tf.io.FixedLenFeature([], tf.string),
        "label": tf.io.FixedLenFeature([], tf.string),
    }

    parsed_features = tf.io.parse_single_example(example_proto, features)
    feature = parsed_features["feature"]
    label = parsed_features["label"]

    feature = tf.io.parse_tensor(feature, out_type=tf.float32)
    label = tf.io.parse_tensor(label, out_type=tf.float32)

    feature = tf.reshape(feature, (100, 100, 1))
    # print(f"LABEL: {tf.shape(label)}")
    # label = tf.keras.utils.to_categorical(label)
    # # label = tf.reshape(label, [5])
    return feature, label


def prepare_dataset(dataset, batch_size, shuffle=False):
    AUTOTUNE = tf.data.AUTOTUNE
    if shuffle:
        dataset = dataset.shuffle(buffer_size=256)
    # dataset = dataset.repeat(20)
    dataset = dataset.prefetch(buffer_size=AUTOTUNE)
    dataset = dataset.batch(batch_size)
    return dataset


def load_tfrecord(tfrecord_path, batch_size=1):
    def decode_example(example_proto):
        dataset = tf.data.TFRecordDataset(tfrecord_path)  # load tfrecord file
        dataset = dataset.map(_parse_function)  # parse data into tensor
        # dataset = dataset.repeat(2) # repeat for 2 epoches
        # dataset = dataset.batch(batch_size)  # set batch_size = 5
        dataset = prepare_dataset(dataset, batch_size, shuffle=False)

        return dataset

    dataset = decode_example(tfrecord_path)
    return dataset


def run(
    trained_model_path,
    trained_model_metrics_path,
):

    # nnlo-insert import
    module = import_util.module_from_spec(spec)
    spec.loader.exec_module(module)

    # nnlo-insert strategy

    # nnlo-insert loss

    with strategy.scope():
        # nnlo-insert load-model

        # nnlo-insert optimizer

        # nnlo-insert epoch
        # nnlo-insert batch-size
        # nnlo-insert callbacks

        train_data = load_tfrecord(
            [
                "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_0-of-12.tfrecord",
                "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_1-of-12.tfrecord",
            ],
            1,
        )
        validation_data = load_tfrecord(
            [
                "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_2-of-12.tfrecord",
                "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_3-of-12.tfrecord",
            ],
            1,
        )
        # train_dataset = strategy.experimental_distribute_dataset(train_dataset)

        # model.compile(
        #     optimizer=model.optimizer(),
        #     loss=model.loss(),
        #     metrics=[None],
        # )

        # model.fit(
        #     train_data,
        #     epochs,
        #     validation_data,
        #     callbacks,
        # )

        for epoch in range(epochs):
            print("\nStart of epoch %d" % (epoch,))
            for step, (x_batch_train, y_batch_train) in enumerate(train_data):
                print(f"x_batch_train: {x_batch_train}\n {x_batch_train.shape}")
                print(f"y_batch_train: {y_batch_train}\n {y_batch_train.shape}")
                # This should be optimized not to be eager, i.e., @tf.function
                with tf.GradientTape() as tape:
                    logits = model(x_batch_train, training=True)
                    loss_value = loss_fn(y_batch_train, logits)
                grads = tape.gradient(loss_value, model.trainable_variables)
                optimizer.apply_gradients(zip(grads, model.trainable_variables))
                # Some metrics could be added at this point
                # Checkpoints also
                # But first...
                # It is time for Irena to shine and add validation logic ;-)

                # Random logging
                if step % 100 == 0:
                    print(
                        "Training loss at step {:3} ({:5} samples): {:.4f}".format(
                            step, (step + 1) * batch_size, float(loss_value)
                        )
                    )

        model.save(trained_model_path)
