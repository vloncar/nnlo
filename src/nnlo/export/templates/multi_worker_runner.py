import argparse
import json
import os
import sys

import tensorflow as tf

# nnlo-insert import

# nnlo-insert config-filename

if __name__ == "__main__":
    # nnlo-insert visible-devices

    parser = argparse.ArgumentParser()
    parser.add_argument("workerid", type=int, help="Worker ID (0, 1, 2...)")
    args = parser.parse_args()

    with open(CONFIG_FILENAME) as json_file:
        tf_config_all = json.load(json_file)
        os.environ["TF_CONFIG"] = json.dumps(
            tf_config_all["cluster_config"][args.workerid]
        )

    strategy = tf.distribute.MultiWorkerMirroredStrategy()

    # nnlo-insert driver

    with strategy.scope():
        model = driver.get_model()
        model.compile(
            optimizer=driver._optimizer,
            loss=driver._loss,
        )

    model.fit(
        driver.train_path,
        epochs=driver._epochs,
        validation_data=driver.valid_path,
    )
