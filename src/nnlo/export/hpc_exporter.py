### This script should export the job script and run it on HPC.
### It should use a template and fill it in accordingly,
### depending on the instance of the class HpcJobInfo.

import os
import pathlib
import tempfile
from datetime import datetime, timedelta
from importlib import util as import_util
import inspect

from nnlo.export.exporter import Exporter
from nnlo.run.runner import Runner
from nnlo.run import TensorflowTrainingRunner


class HpcJobInfo():
    """HpcJobInfo class is a class which has to be implemented in order to have 
       the crucial information about the running the training on HPC.
    """

    def __init__(self, account, nodes, ntasks, ntasks_per_node, output, error, time, partition, gres, job_name, folder, modules, venv, script_name):
        self._account = account
        self._nodes = nodes
        self._ntasks = ntasks
        self._ntasks_per_node = ntasks_per_node
        self._output = output
        self._error = error
        self._time = time
        self._partition = partition
        self._gres = gres
        self._job_name = job_name
        self._folder = folder
        self._modules = modules
        self._venv = venv
        self._script_name = script_name

    @property
    def account(self):
        return self._account

    @property
    def nodes(self):
        return self._nodes

    @property
    def ntasks(self):
        return self._ntasks

    @property
    def output(self):
        return self._output

    @property
    def error(self):
        return self._error

    @property
    def time(self):
        return self._time

    @property
    def partition(self):
        return self._partition

    @property
    def gres(self):
        return self._gres

    @property
    def job_name(self):
        return self._job_name

    @property
    def folder(self):
        return self._folder

    @property
    def modules(self):
        return self._modules

    @property
    def venv(self):
        return self._venv

    @property
    def script_name(self):
        return self._script_name


class HpcExporter(Exporter):
    def _write_runner(self, template_file, output_file_name, driver, hpc_job_info):
        # Writing shall script for running training on HPC site.

        if inspect.isclass(driver):
            driver_class = driver
        else:
            driver_class = driver.__class__
        driver_cls_file = inspect.getfile(driver_class)
        driver_path = os.path.abspath(driver_cls_file)

        template = open(template_file, "r")
        output_file = open(f"{output_file_name}", "w")

        output_path = os.path.abspath(output_file.name)
        project_path = os.path.dirname(output_path) + "/" + driver.get_project_name()

        for line in template.readlines():

            if "# nnlo-insert account" in line:
                newline = f"#SBATCH --account=" + hpc_job_info.account

            elif "# nnlo-insert nodes" in line:
                newline = f"#SBATCH --nodes=" + str(hpc_job_info.nodes)

            elif "# nnlo-insert ntasks" in line:
                newline = f"#SBATCH --ntasks=" + str(hpc_job_info.ntasks)

            elif "# nnlo-insert ntasks-per-node" in line:
                newline = f"#SBATCH --ntasks-per-node=" + str(hpc_job_info.ntasks_per_node)

            elif "# nnlo-insert output" in line:
                newline = f"#SBATCH --output=" + hpc_job_info.output + "-%j.out"

            elif "# nnlo-insert error" in line:
                newline = f"#SBATCH --error=" + hpc_job_info.error + "-%j.err"

            elif "# nnlo-insert time" in line:
                newline = f"#SBATCH --time=" + hpc_job_info.time

            elif " nnlo-insert partition" in line:
                newline = f"#SBATCH --partition=" + hpc_job_info.partition

            elif "# nnlo-insert gres" in line:
                newline = f"#SBATCH --gres=gpu:" + str(hpc_job_info.gres)

            elif "# nnlo-insert job-name" in line:
                newline = f"#SBATCH --job-name=" + hpc_job_info.job_name + "-%j" + "\n"

            elif "# nnlo-insert folder" in line:
                newline = f"cd " + hpc_job_info.folder + "\n"

            elif "# nnlo-insert modules" in line:
                newline = f"module load " + hpc_job_info.modules + "\n"

            elif "# nnlo-insert venv" in line:
                newline = f"source " + hpc_job_info.venv + "\n"

            elif "# nnlo-insert srun" in line:
                newline = f"srun python {hpc_job_info.script_name} \n"

            else:
                newline = line

            output_file.write(newline)

        template.close()
        output_file.close()

        return output_path

    def export(
        self,
        driver,
        hpc_job_info,
        epochs=10,
        output_file_name="hpc_runner_001.sh",
    ):
        if not output_file_name.endswith(".sh"):
            output_file_name += ".sh"

        filedir = os.path.dirname(os.path.abspath(__file__))
        template_file = os.path.join(filedir, "templates/hpc_job_runner.sh")
        module_path = self._write_runner(template_file, output_file_name, driver, hpc_job_info)

        hpc_runner = _HpcRunner(module_path, driver, epochs)

        return hpc_runner.run()  # TODO check if it is okay like this


class _HpcRunner(Runner):
    def __init__(self, job_script_name, driver, epochs):
        super().__init__(None)
        self.job_script_name = job_script_name
        self.driver = driver
        self.epochs = epochs

    def run(self):
        if "login" in os.environ['HOSTNAME']:
            # Login node, submit a job.
            os.system(f'sbatch {self.job_script_name}')
        else:
            # Computing node, run a training.
            runner = TensorflowTrainingRunner(self.driver, self.epochs)
            model = runner.run()

            # Evaluate the trained model
            score = model.evaluate(self.driver.get_validation_data(), verbose=0)
            print("Test loss: ", score[0])
            print("Test accuracy: ", score[1])
