from abc import ABC, abstractmethod

import tensorflow as tf


class Runner(ABC):
    def __init__(self, driver):
        self.driver = driver

    @abstractmethod
    def run(self):
        pass


loss_mapping_keras = {
    "categorical_crossentropy": tf.keras.losses.CategoricalCrossentropy(
        from_logits=True
    ),
    "sparse_categorical_crossentropy": tf.keras.losses.SparseCategoricalCrossentropy(
        from_logits=True
    ),
}


class TensorflowTrainingRunner(Runner):
    def __init__(self, driver, epochs=10, save_path=None):
        super().__init__(driver)
        self.epochs = epochs
        self.save_path = save_path

    def _run_single(self):
        model = self.driver.get_model()
        model.compile(
            optimizer=self.driver.get_optimizer(),
            loss=self.driver.get_loss(),
            metrics=self.driver.get_metrics(),
        )

        train_data = self.driver.get_train_data()
        if isinstance(train_data, tuple):
            x_train_data = train_data[0]
            y_train_data = train_data[1]
        else:
            x_train_data = train_data
            y_train_data = None

        model.fit(
            x=x_train_data,
            y=y_train_data,
            batch_size=self.driver.get_batch_size(),
            epochs=self.epochs,
            validation_data=self.driver.get_validation_data(),
            callbacks=self.driver.get_callbacks(),
        )

        return model

    def _run_distributed(self):
        strategy = self.driver.get_dist_strategy().to_tf_strategy()

        with strategy.scope():
            model = self.driver.get_model()
            model.compile(
                optimizer=self.driver.get_optimizer(),
                loss=self.driver.get_loss(),
                metrics=self.driver.get_metrics(),
            )

            train_data = self.driver.get_train_data()
            if isinstance(train_data, tuple):
                x_train_data = train_data[0]
                y_train_data = train_data[1]
            else:
                x_train_data = train_data
                y_train_data = None

            model.fit(
                x=x_train_data,
                y=y_train_data,
                batch_size=self.driver.get_batch_size(),
                epochs=self.epochs,
                validation_data=self.driver.get_validation_data(),
                callbacks=self.driver.get_callbacks(),
            )

        return model

    def run(self):
        if self.driver.get_dist_strategy() is None:
            model = self._run_single()
        else:
            model = self._run_distributed()

        if self.save_path is not None:
            model.save(self.save_path)
        
        return model

