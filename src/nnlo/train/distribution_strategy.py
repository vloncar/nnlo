import json
from abc import ABC, abstractmethod

import tensorflow as tf


class DistributionStrategy(ABC):
    @abstractmethod
    def to_tf_strategy(self):
        raise NotImplementedError


class SingleNodeStrategy(DistributionStrategy):
    def __init__(self, n_copies=-1) -> None:
        self.n_copies = n_copies

    def to_tf_strategy(self):
        if self.n_copies <= 0:
            return tf.distribute.MirroredStrategy()

        gpu_config = []
        for index in range(self.n_copies):
            gpu_config.append(f"GPU:{index}")

        return tf.distribute.MirroredStrategy(gpu_config)


def _check_ports(worker_list):
    for i in range(len(worker_list)):
        if ":" not in worker_list[i]:
            worker_list[i] = worker_list[i] + ":14994"


class MultiNodeStrategy(DistributionStrategy):
    def __init__(self, worker_list, visible_devices=None) -> None:
        # TODO visible_devices should be list of lists and do proper checks, len(visible_devices) == len(worker_list)
        self.worker_list = (
            worker_list  # [hostname_or_ip[:port], hostname_or_ip[:port], ...]
        )
        self.visible_devices = visible_devices

    def to_tf_strategy(self):
        raise Exception(
            "Cannot create strategy object directly, please use the proper exporter."
        )

    @property
    def visible_devices_env(self):
        if self.visible_devices is not None:
            return ",".join([str(d) for d in self.visible_devices])

    @property
    def tf_config(self):
        # Creating dictionary which contains configs for all nodes

        cluster_dict = {"cluster_config": []}

        _check_ports(self.worker_list)

        list_nodes_dict = []

        for i in range(len(self.worker_list)):
            temp_node_dict = {
                "cluster": {"worker": []},
                "task": {"type": "worker", "index": 0},
            }
            temp_node_dict["cluster"]["worker"] = self.worker_list
            temp_node_dict["task"]["index"] = i
            list_nodes_dict.append(temp_node_dict)

        cluster_dict["cluster_config"] = list_nodes_dict

        cluster_json = json.dumps(cluster_dict)

        return cluster_json
