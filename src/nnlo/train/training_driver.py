from abc import ABC, abstractmethod
from nnlo.data.dataset import DatasetGenerator

from nnlo.train.distribution_strategy import SingleNodeStrategy


class TrainingDriver(ABC):
    # Abstract class TrainingDriver, all methods are abstract

    @abstractmethod
    def get_model(self):
        pass

    @abstractmethod
    def get_project_name(self):
        pass

    @abstractmethod
    def get_train_data(self):
        pass

    @abstractmethod
    def get_validation_data(self):
        pass

    def get_batch_size(self):
        return None

    @abstractmethod
    def get_loss(self):
        pass

    @abstractmethod
    def get_optimizer(self):
        pass

    @abstractmethod
    def get_metrics(self):
        pass

    @abstractmethod
    def get_dist_strategy(self):
        pass

    @abstractmethod
    def get_callbacks(self):
        pass

class TensorflowTrainingDriver(TrainingDriver):
    def __init__(self, project_name, model_builder, train_data, validation_data=None, batch_size=None, loss=None, optimizer=None, metrics=None, callbacks=None, dist_trategy=None):
        self._project_name = project_name
        self._model_builder = model_builder
        self._set_data(train_data, validation_data, batch_size)
        self._loss = loss
        self._optimizer = optimizer
        self._metrics = metrics
        self._callbacks = callbacks
        self._dist_strategy = dist_trategy

    def _set_data(self, train_data, validation_data, batch_size):
        self._batch_size = None
        if isinstance(train_data, DatasetGenerator):
            self._train_data = train_data.get_train_dataset()
            self._validation_data = train_data.get_validation_dataset()
            
        else:
            self._train_data = train_data
            self._validation_data = validation_data
            if isinstance(self._train_data, tuple):
                self._batch_size = batch_size

    def get_project_name(self):
        return self._project_name

    def get_model(self):
        return self._model_builder()

    def get_train_data(self):
        return self._train_data

    def get_validation_data(self):
        return self._validation_data

    def get_batch_size(self):
        return self._batch_size

    def get_loss(self):
        return self._loss

    def get_optimizer(self):
        return self._optimizer

    def get_metrics(self):
        return self._metrics

    def get_dist_strategy(self):
        return self._dist_strategy

    def get_callbacks(self):
        return self._callbacks
