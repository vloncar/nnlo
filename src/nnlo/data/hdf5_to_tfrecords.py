import glob
import logging
from abc import ABC, abstractmethod

import h5py
import numpy as np
import tensorflow as tf
from tensorflow.keras import backend as K


def _bytes_feature(value):
    """Returns a bytes_list from a string / byte."""
    if isinstance(value, type(tf.constant(0))):
        value = value.numpy()  # BytesList won't unpack a string from an EagerTensor.
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _float_feature(value):
    """Returns a float_list from a float / double."""
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


def _int64_feature(value):
    """Returns an int64_list from a bool / enum / int / uint."""
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _serialise_array(array):
    """Transforms the data into tf.Tensor"""
    array = tf.io.serialize_tensor(array)
    return array


def serialise_example(feature, label):
    """Creates a tf.train.Example message ready to be written to a file."""
    # Create a dictionary mapping the feature name to the tf.train.Example-compatible
    # data type.

    feature = {"feature": _bytes_feature(feature), "label": _bytes_feature(label)}

    # Create a Features message using tf.train.Example.
    example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
    return example_proto


def generate_tfrecord(tfrecord_file, features, labels, n_samples):
    """Creates tfrecord"""

    with tf.io.TFRecordWriter(tfrecord_file) as writer:
        for i in range(n_samples):
            tf_example = serialise_example(
                _serialise_array(features[i]), _serialise_array(labels[i])
            )
            writer.write(tf_example.SerializeToString())
