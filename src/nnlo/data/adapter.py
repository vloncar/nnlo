import glob
import logging
from abc import ABC, abstractmethod

import h5py
import numpy as np


class DataAdapter(ABC):
    def sample_shape(self):
        raise NotImplementedError

    def label_shape(self):
        raise NotImplementedError

    def read_next(self):
        raise NotImplementedError

    def __iter__(self):
        return self

    def __next__(self):
        return self.read_next()


class NumpyAdapter(DataAdapter):
    def __init__(self, features, labels):
        self.features = features
        self.labels = labels
        self.num = 0

    def sample_shape(self):
        return self.features.shape[1:]

    def label_shape(self):
        return self.labels.shape[1:]

    def read_next(self):
        self.num += 1
        return self.features[self.num - 1], self.labels[self.num - 1]


class HdfAdapter(DataAdapter):
    def __init__(
        self,
        feature_group,
        label_group,
        glob_pattern="*.h5",
        feature_dtype=None,
        label_dtype=None,
    ):
        self.current_file = None
        self.glob_pattern = glob_pattern
        self.list_files = glob.glob(self.glob_pattern)
        self.generator = self._read_next()
        self.feature_group = feature_group
        self.label_group = label_group
        if callable(feature_group):
            self.feature_group_accessor = feature_group
        else:
            self.feature_group_accessor = lambda f: f[self.feature_group]
        if callable(label_group):
            self.label_group_accessor = label_group
        else:
            self.label_group_accessor = lambda f: f[self.label_group]
        self.feature_dtype = feature_dtype
        self.label_dtype = label_dtype

    def _read_next(self):
        for file in self.list_files:
            self.current_file = file
            logging.debug(f"Processing file: {self.current_file} ...")
            f = h5py.File(self.current_file)

            feature = np.array(self.feature_group_accessor(f), dtype=self.feature_dtype)
            label = np.array(self.label_group_accessor(f), dtype=self.label_dtype)

            feature.tolist()
            label.tolist()

            for feat, lab in zip(feature, label):
                yield (feat, lab)

    def __iter__(self):
        return self

    def __next__(self):
        return next(self.generator)
