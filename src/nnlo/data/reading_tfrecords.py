import logging

import tensorflow as tf


def _parse_function(example_proto):
    features = {
        "feature": tf.io.FixedLenFeature([], tf.string),
        "label": tf.io.FixedLenFeature([], tf.string),
    }

    parsed_features = tf.io.parse_single_example(example_proto, features)
    feature = parsed_features["feature"]
    label = parsed_features["label"]

    feature = tf.io.parse_tensor(feature, out_type=tf.float32)
    label = tf.io.parse_tensor(label, out_type=tf.float32)

    feature = tf.reshape(feature, (100, 100, 1))
    # print(f"LABEL: {tf.shape(label)}")
    # label = tf.keras.utils.to_categorical(label)
    # # label = tf.reshape(label, [5])
    return feature, label


def prepare_dataset(dataset, batch_size, shuffle=False):
    AUTOTUNE = tf.data.AUTOTUNE
    if shuffle:
        dataset = dataset.shuffle(buffer_size=256)
    # dataset = dataset.repeat(20)
    dataset = dataset.prefetch(buffer_size=AUTOTUNE)
    dataset = dataset.batch(batch_size)
    return dataset


def load_tfrecord(tfrecord_path, batch_size=1):
    def decode_example(example_proto):
        dataset = tf.data.TFRecordDataset(tfrecord_path)  # load tfrecord file
        dataset = dataset.map(_parse_function)  # parse data into tensor
        # dataset = dataset.repeat(2) # repeat for 2 epoches
        # dataset = dataset.batch(batch_size)  # set batch_size = 5
        dataset = prepare_dataset(dataset, batch_size, shuffle=False)

        return dataset

    dataset = decode_example(tfrecord_path)
    return dataset
