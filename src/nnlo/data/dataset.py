import logging
from pathlib import Path

import numpy as np
import tensorflow as tf


def _bytes_feature(value):
    """Returns a bytes_list from a string / byte."""
    if isinstance(value, type(tf.constant(0))):
        value = value.numpy()  # BytesList won't unpack a string from an EagerTensor.
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _float_feature(value):
    """Returns a float_list from a float / double."""
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


def _int64_feature(value):
    """Returns an int64_list from a bool / enum / int / uint."""
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _serialise_array(array):
    """Transforms the data into tf.Tensor"""
    array = tf.io.serialize_tensor(array)
    return array


def serialise_example(feature, label):
    """Creates a tf.train.Example message ready to be written to a file."""
    # Create a dictionary mapping the feature name to the tf.train.Example-compatible
    # data type.

    feature = {"feature": _bytes_feature(feature), "label": _bytes_feature(label)}

    # Create a Features message using tf.train.Example.
    example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
    return example_proto


def generate_tfrecord(tfrecord_file, features, labels, n_samples):
    """Creates tfrecord"""

    with tf.io.TFRecordWriter(tfrecord_file) as writer:
        for i in range(n_samples):
            tf_example = serialise_example(
                _serialise_array(features[i]), _serialise_array(labels[i])
            )
            writer.write(tf_example.SerializeToString())


class DatasetGenerator(object):
    def __init__(self, name, train_adapter, test_adapter, output_path):
        self.name = name
        self.train_adapter = train_adapter
        self.test_adapter = test_adapter
        self.output_path = output_path

    def generate_tf_dataset(self, num_samples, num_records=10):
        """Generate tf.data.Dataset dataset from the given adapter(s)

        Args:
            train_adapter (nnlo.data.DataAdapter): Adapter for training data.
            test_adapter (nnlo.data.DataAdapter): Adapter for test data. If None ..., e
            num_samples (int or tuple): _description_
            num_records (int, optional): _description_. Defaults to 10.
            output_path (_type_, optional): _description_. Defaults to None.
        """
        images = list()
        labels = list()

        tf_samples = num_samples // num_records
        tf_samples_last = tf_samples + num_samples % num_records

        num = 0
        num_tf = 0
        for im, lab in self.train_adapter:  # TODO handle test data, if any
            images.append(np.array(im))
            labels.append(np.array(lab))

            num += 1
            if (num == tf_samples) and (num_tf != num_records):
                num_tf += 1
                root_path = Path(self.output_path)
                tfrecords_folder = (root_path / "tfrecords").resolve()
                Path(tfrecords_folder).mkdir(parents=True, exist_ok=True)
                tfrecord_file = (
                    tfrecords_folder / f"{self.name}_{num_tf}-of-{num_records}.tfrecord"
                )
                generate_tfrecord(str(tfrecord_file), images, labels, num)
                logging.debug(
                    f"TFRecord {self.name}_{num_tf}-of-{num_records}.tfrecord is created!"
                )

                num = 0
                images = list()
                labels = list()

            # Last TFRecord
            if (num == tf_samples_last) and (num_tf == num_records):
                num_tf += 1
                root_path = Path(self.output_path)
                tfrecords_folder = (root_path / "tfrecords").resolve()
                Path(tfrecords_folder).mkdir(parents=True, exist_ok=True)
                tfrecord_file = (
                    tfrecords_folder / f"{self.name}_{num_tf}-of-{num_records}.tfrecord"
                )
                generate_tfrecord(str(tfrecord_file), images, labels, num)
                logging.debug(
                    f"TFRecord {self.name}_{num_tf}-of-{num_records}.tfrecord is created!"
                )

    def get_train_dataset(self):
        # TODO load from self.output_path
        pass
        # At the end, create tf.data.Dataset

    def get_validation_dataset(self):
        # TODO load from self.output_path
        pass
        # At the end, create tf.data.Dataset

