import json
import os

import tensorflow as tf

from nnlo.data.reading_tfrecords import load_tfrecord
from nnlo.export import MultiWorkerExporter
from nnlo.train import (
    DefaultFeatures,
    DefaultLabels,
    DefaultLearningRate,
    DefaultMetrics,
    DefaultOptimizer,
    DefaultTenflo,
    DefaultTorch,
    MultiNodeStrategy,
    TrainingDriver,
)


class JetDistStrategy(TrainingDriver):
    def get_dist_strategy(self):
        return MultiNodeStrategy(
            worker_list=["10.3.10.20:12345", "10.3.20.104"], visible_devices=[2]
        )


class JetModel(TrainingDriver):
    def get_model(self):
        model = tf.keras.Sequential(
            [
                tf.keras.layers.Input(shape=(100, 100, 1)),
                tf.keras.layers.Conv2D(
                    kernel_size=(5, 5),
                    filters=5,
                    strides=(1, 1),
                    padding="same",
                    data_format="channels_last",
                ),
                tf.keras.layers.BatchNormalization(),
                tf.keras.layers.Activation("relu"),
                tf.keras.layers.MaxPooling2D(pool_size=(5, 5)),
                tf.keras.layers.Dropout(0.25),
                tf.keras.layers.Conv2D(
                    kernel_size=(3, 3), filters=3, strides=(1, 1), padding="same"
                ),
                tf.keras.layers.BatchNormalization(),
                tf.keras.layers.Activation("relu"),
                tf.keras.layers.MaxPooling2D(pool_size=(3, 3)),
                tf.keras.layers.Dropout(0.25),
                tf.keras.layers.Flatten(),
                tf.keras.layers.Dense(5, "relu"),
                tf.keras.layers.Dense(5, "softmax"),
            ]
        )
        return model


class JetProjectName(TrainingDriver):
    def get_project_name(self):
        return "Jet_project-Multi_Workers"


class JetEpochs(TrainingDriver):
    def get_epochs(self):
        return 3


class JetBatchSize(TrainingDriver):
    def get_batch_size(self):
        return 32


class JetTrainPath(TrainingDriver):
    def get_train_set(self):
        train_filenames = [
            "/storage/af/user/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_00-of-12.tfrecord",
            "/storage/af/user/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_01-of-12.tfrecord",
            "/storage/af/user/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_02-of-12.tfrecord",
            "/storage/af/user/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_03-of-12.tfrecord",
            "/storage/af/user/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_04-of-12.tfrecord",
            "/storage/af/user/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_05-of-12.tfrecord",
            "/storage/af/user/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_06-of-12.tfrecord",
            "/storage/af/user/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_07-of-12.tfrecord",
            # "/storage/af/user/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_08-of-12.tfrecord",
        ]

        return train_filenames


class JetValidPath(TrainingDriver):
    def get_valid_set(self):
        valid_filenames = [
            "/storage/af/user/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_09-of-12.tfrecord",
            "/storage/af/user/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_10-of-12.tfrecord",
            "/storage/af/user/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_11-of-12.tfrecord",
            "/storage/af/user/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_12-of-12.tfrecord",
        ]

        return valid_filenames


class JetLoss(TrainingDriver):
    def get_loss(self):
        return "categorical_crossentropy"


class JetTrainingDriver(
    JetModel,
    JetProjectName,
    JetTrainPath,
    JetValidPath,
    DefaultFeatures,
    DefaultLabels,
    JetLoss,
    DefaultOptimizer,
    DefaultLearningRate,
    JetEpochs,
    JetBatchSize,
    DefaultMetrics,
    JetDistStrategy,
    DefaultTenflo,
    DefaultTorch,
):
    def __init__(self):
        self._model = self.get_model()
        self._project_name = self.get_project_name()
        self._train_path = load_tfrecord(self.get_train_set(), self.get_batch_size())
        self._valid_path = load_tfrecord(self.get_valid_set(), self.get_batch_size())
        self._features = self.get_features
        self._labels = self.get_labels
        self._epochs = self.get_epochs()
        self._batch_size = self.get_batch_size()
        self._loss = self.get_loss()
        self._optimizer = self.get_optimizer()
        self._learning_rate = self.get_learning_rate()
        self._metrics = self.get_metrics()
        self._dist_strategy = self.get_dist_strategy()
        self._tenflo = self.get_tenflo()
        self._tor = self.get_tor()

    @property
    def train_path(self):
        return self._train_path

    @property
    def valid_path(self):
        return self._valid_path


if __name__ == "__main__":

    driver = JetTrainingDriver()

    exporter = MultiWorkerExporter()
    exporter.export(driver, "multi_worker_runner_1.py", "nnlo_worker_config_1.json")
