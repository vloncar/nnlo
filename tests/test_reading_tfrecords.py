# Test reading TFRecord files.

import logging

import pytest

from nnlo.data import load_tfrecord


def test_reading_tfrecords():
    logging.basicConfig(
        format="%(asctime)s | %(levelname)s: %(message)s", level=logging.NOTSET
    )

    tfrecord_path = [
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_10-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_11-of-12.tfrecord",
    ]
    batch_size = 1
    dataset = load_tfrecord(tfrecord_path, batch_size)
    logging.debug(f"Iterating through tfrecord: {tfrecord_path}...")
    num = 0
    for image, label in dataset:
        image = image.numpy()
        label = label.numpy()
        label = label[0]
        image = image[0]
        assert label.shape == (5,)
        assert image.shape == (100, 100, 1)
        num += 1

    logging.debug(f"Reading of the TFRecord: {tfrecord_path} is finished!")
    logging.debug(f"Number of samples: {num}")


if __name__ == "__main__":
    test_reading_tfrecords()
