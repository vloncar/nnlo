import argparse
import json
import os

import mnist
import tensorflow as tf

os.environ["CUDA_VISIBLE_DEVICES"] = "1"

parser = argparse.ArgumentParser()
parser.add_argument("workerid", type=int, help="Worker ID (0, 1, 2...)")
args = parser.parse_args()

os.environ.pop("TF_CONFIG", None)
tf_config = {
    "cluster": {"worker": ["10.3.10.20:12345", "10.3.20.104:23456"]},
    "task": {"type": "worker", "index": 0},
}

tf_config["task"]["index"] = args.workerid

print(json.dumps(tf_config))
s = json.dumps(tf_config)

os.environ["TF_CONFIG"] = s

per_worker_batch_size = 64
tf_config = json.loads(os.environ["TF_CONFIG"])
num_workers = len(tf_config["cluster"]["worker"])

strategy = tf.distribute.MultiWorkerMirroredStrategy()

global_batch_size = per_worker_batch_size * num_workers
multi_worker_dataset = mnist.mnist_dataset(global_batch_size)

with strategy.scope():
    multi_worker_model = mnist.build_and_compile_cnn_model()


multi_worker_model.fit(multi_worker_dataset, epochs=3, steps_per_epoch=70)
