# Example training Conv net using MNIST dataset.
# It is distributed training, single-node, multiple-gpu, using NNLO lib.
# Training is done on the HPC resources.
# The parameters important for submitting the job on HPC computing nodes
# are indicated in the class HpcJobInfo.
# The shell script is generated and automatically run.
# The user must be logged in to the login node of the HPC in order to run this test.

import os
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from nnlo.export.hpc_exporter import HpcExporter, HpcJobInfo
from nnlo.run import TensorflowTrainingRunner
from nnlo.train import TensorflowTrainingDriver
from nnlo.train import SingleNodeStrategy

# Prepare the data

# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# the data, split between train and test sets
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# Scale images to the [0, 1] range
x_train = x_train.astype("float32") / 255
x_test = x_test.astype("float32") / 255
# Make sure images have shape (28, 28, 1)
x_train = np.expand_dims(x_train, -1)
x_test = np.expand_dims(x_test, -1)
print("x_train shape:", x_train.shape)
print(x_train.shape[0], "train samples")
print(x_test.shape[0], "test samples")


# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)


# Build the model

def build_model():
    model = keras.Sequential(
        [
            keras.Input(shape=input_shape),
            layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
            layers.MaxPooling2D(pool_size=(2, 2)),
            layers.Flatten(),
            layers.Dropout(0.5),
            layers.Dense(num_classes, activation="softmax"),
        ]
    )

    return model

# Train the model


batch_size = 128
epochs = 15

# Preventing warnings:
# Consider either turning off auto-sharding or switching the auto_shard_policy to DATA to shard this dataset.
# You can do this by creating a new `tf.data.Options()` object then setting
# `options.experimental_distribute.auto_shard_policy = AutoShardPolicy.DATA` before applying the options
# object to the dataset via `dataset.with_options(options)`.

train_data = tf.data.Dataset.from_tensor_slices((x_train, y_train))
val_data = tf.data.Dataset.from_tensor_slices((x_test, y_test))

train_data = train_data.batch(batch_size)
val_data = val_data.batch(batch_size)

# Disable AutoShard.
options = tf.data.Options()
options.experimental_distribute.auto_shard_policy = tf.data.experimental.AutoShardPolicy.DATA
train_data = train_data.with_options(options)
val_data = val_data.with_options(options)

driver = TensorflowTrainingDriver("nnlo_cnn_mnist", build_model, train_data,
                                  val_data, loss="categorical_crossentropy", metrics=["accuracy"],
                                  optimizer="adam", dist_trategy=SingleNodeStrategy())


#hpc_job = HpcJobInfo(account="Pra24_0016", nodes=1, ntasks=1, ntasks_per_node=1, output="nnlo", error="nnlo", time="00:10:00",
#                     partition="m100_usr_prod", gres=4, job_name="test_job", folder="~/nnlo/examples", modules="profile/deeplrn autoload tensorflow/2.3.0--cuda--10.2",
#                     venv="~/env-tf-nnlo/bin/activate", script_name="cnn_mnist.py")

#exporter = HpcExporter()
#exporter.export(driver, hpc_job)

script_name = os.path.basename(__file__)
hpc_job = HpcJobInfo(account="Pra24_0016", nodes=1, ntasks=1, ntasks_per_node=1, output="nnlo", error="nnlo", time="00:10:00",
                     partition="m100_usr_prod", gres=4, job_name="test_job", folder="~/nnlo/tests", modules="profile/deeplrn autoload tensorflow/2.3.0--cuda--10.2",
                     venv="~/env-tf-nnlo/bin/activate", script_name=script_name)

exporter = HpcExporter()
exporter.export(driver, hpc_job, epochs)
