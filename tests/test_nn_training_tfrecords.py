import os
from pathlib import Path

import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.python.keras.callbacks import (
    EarlyStopping,
    ReduceLROnPlateau,
    TerminateOnNaN,
)

from nnlo.data.reading_tfrecords import load_tfrecord


def get_model():
    model = tf.keras.Sequential(
        [
            tf.keras.layers.Input(shape=(100, 100, 1)),
            tf.keras.layers.Conv2D(
                kernel_size=(5, 5),
                filters=5,
                strides=(1, 1),
                padding="same",
                data_format="channels_last",
            ),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Activation("relu"),
            tf.keras.layers.MaxPooling2D(pool_size=(5, 5)),
            tf.keras.layers.Dropout(0.25),
            tf.keras.layers.Conv2D(
                kernel_size=(3, 3), filters=3, strides=(1, 1), padding="same"
            ),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Activation("relu"),
            tf.keras.layers.MaxPooling2D(pool_size=(3, 3)),
            tf.keras.layers.Dropout(0.25),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(5, "relu"),
            tf.keras.layers.Dense(5, "softmax"),
        ]
    )

    return model


def test_training():
    os.environ["CUDA_VISIBLE_DEVICES"] = "2"

    train_filenames = [
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_0-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_1-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_2-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_3-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_4-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_5-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_6-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_7-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_8-of-12.tfrecord",
    ]

    valid_filenames = [
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_9-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_10-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_11-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_12-of-12.tfrecord",
    ]
    batch_size = 128
    epochs = 3

    model = get_model()

    model.summary()

    model.compile(
        optimizer=tf.keras.optimizers.Adam(),
        loss=tf.keras.losses.CategoricalCrossentropy(),
    )

    history = model.fit(
        x=load_tfrecord(train_filenames, batch_size),
        epochs=epochs,
        batch_size=batch_size,
        verbose=2,
        validation_data=load_tfrecord(valid_filenames, batch_size),
        callbacks=[
            EarlyStopping(monitor="val_loss", patience=10, verbose=1),
            ReduceLROnPlateau(monitor="val_loss", factor=0.1, patience=2, verbose=1),
            TerminateOnNaN(),
        ],
    )

    plt.plot(history.history["loss"])
    plt.plot(history.history["val_loss"])
    plt.yscale("log")
    plt.title("Training history")
    plt.ylabel("loss")
    plt.xlabel("epoch")
    plt.legend(["training", "validation"], loc="upper right")
    plt.show()
    root_path = Path(__file__).parent
    plots_folder = Path(root_path / "plots").resolve()
    plots_folder.mkdir(parents=True, exist_ok=True)
    Path("plots").mkdir(parents=True, exist_ok=True)
    plt.savefig(plots_folder / "plot.png")


if __name__ == "__main__":
    test_training()
