# Test for HDF adapter.

from pathlib import Path

import numpy as np
import pytest

from nnlo.data import HdfAdapter


def test_hdf_read():
    root_path = Path(__file__).parent
    h5_files = (root_path / "../../tutorials/Data/JetDataset/*.h5").resolve()
    da = HdfAdapter(
        "jetImage",
        lambda f: f["jets"][0:, -6:-1],
        str(h5_files),
        np.float32,
        np.float32,
    )
    for im, lab in da:
        assert len(im) == 100
        assert len(lab) == 5


if __name__ == "__main__":
    test_hdf_read()
