# Test for comparing whether the data stored in generated TFRecords are equivalent to data from original H5 files.

import glob
import logging
from pathlib import Path

import numpy as np
import pytest

from nnlo.data import HdfAdapter, load_tfrecord


def test_equivalance():
    logging.basicConfig(
        format="%(asctime)s | %(levelname)s: %(message)s", level=logging.NOTSET
    )

    root_path = Path(__file__).parent
    h5_files = (root_path / "../../tutorials/Data/JetDataset/*.h5").resolve()
    da = HdfAdapter(
        "jetImage",
        lambda f: f["jets"][0:, -6:-1],
        str(h5_files),
        np.float32,
        np.float32,
    )

    batch_size = 1

    # Iterate through TFRecords files and in paralel iterating through h5 files, compare samples
    list_tfrecord_files = [
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_0-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_1-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_2-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_3-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_4-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_5-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_6-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_7-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_8-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_9-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_10-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_11-of-12.tfrecord",
        "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_12-of-12.tfrecord",
    ]

    logging.debug(f"List of TFRecord files: {list_tfrecord_files}")
    for tfrecord_file in list_tfrecord_files:
        dataset = load_tfrecord(tfrecord_file, batch_size)
        logging.debug(f"Iterating through tfrecord: {tfrecord_file}...")
        for tf_image, tf_label in dataset:
            tf_image = tf_image.numpy()
            tf_label = tf_label.numpy()
            tf_label = tf_label[0]
            tf_image = tf_image[0]

            h5_image, h5_label = next(da)

            assert tf_label.shape == h5_label.shape
            assert tf_image.shape == (np.expand_dims(h5_image, axis=-1)).shape
            assert tf_label.all() == h5_label.all()
            assert tf_image.all() == h5_image.all()

        logging.debug(f"Reading of the TFRecord: {tfrecord_file} is finished!")
    logging.debug(f"The datasets in h5 and TFRecord files are equivalent! :)")


if __name__ == "__main__":
    test_equivalance()
