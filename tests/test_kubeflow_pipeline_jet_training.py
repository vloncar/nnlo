from test_jet_dist_training import JetTrainingDriver

from nnlo.export.kubeflow_exporter import CERNKubeflowExporter

if __name__ == "__main__":
    driver = JetTrainingDriver()
    exporter = CERNKubeflowExporter()
    runner = exporter.export(driver, "irena-veljanovic")
    runner.run()
