# Test for generating TFRecords from H5 files.

import logging
from pathlib import Path

import numpy as np
import pytest

from nnlo.data import HdfAdapter, generate_tfrecord


def test_hdf_tf_record_generation():
    logging.basicConfig(
        format="%(asctime)s | %(levelname)s: %(message)s", level=logging.NOTSET
    )

    root_path = Path(__file__).parent
    h5_files = (root_path / "../../tutorials/Data/JetDataset/*.h5").resolve()

    # Label: [0:,-6:-1]
    da = HdfAdapter(
        "jetImage",
        lambda f: f["jets"][0:, -6:-1],
        str(h5_files),
        np.float32,
        np.float32,
    )
    da_iter = iter(da)
    num = 0
    images = list()
    labels = list()
    num_tf = 0
    total_num_tf = 13
    total_num_samples = 80000

    tf_samples = total_num_samples // total_num_tf
    tf_samples_last = tf_samples + total_num_samples % total_num_tf

    for im, lab in da:
        assert im.shape == (100, 100)
        assert lab.shape == (5,)

        images.append(np.array(im))
        labels.append(np.array(lab))

        num += 1
        if (num == tf_samples) and (num_tf != total_num_tf - 1):
            num_tf += 1
            root_path = Path(__file__).parent
            tfrecords_folder = (root_path / "tfrecords_trial_10").resolve()
            Path(tfrecords_folder).mkdir(parents=True, exist_ok=True)
            tfrecord_file = (
                tfrecords_folder / f"jet_data_{num_tf-1}-of-{total_num_tf-1}.tfrecord"
            )
            generate_tfrecord(str(tfrecord_file), images, labels, num)
            logging.debug(
                f"TFRecord jet_data_{num_tf-1}-of-{total_num_tf-1}.tfrecord is created!"
            )

            num = 0
            images = list()
            labels = list()

        # Last TFRecord
        if (num == tf_samples_last) and (num_tf == total_num_tf - 1):
            num_tf += 1
            root_path = Path(__file__).parent
            tfrecords_folder = (root_path / "tfrecords_trial_10").resolve()
            Path(tfrecords_folder).mkdir(parents=True, exist_ok=True)
            tfrecord_file = (
                tfrecords_folder / f"jet_data_{num_tf-1}-of-{total_num_tf-1}.tfrecord"
            )
            generate_tfrecord(str(tfrecord_file), images, labels, num)
            logging.debug(
                f"TFRecord jet_data_{num_tf-1}-of-{total_num_tf-1}.tfrecord is created!"
            )


if __name__ == "__main__":
    test_hdf_tf_record_generation()
