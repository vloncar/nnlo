from test_jet_dist_training import JetTrainingDriver

from nnlo.export.local_custom_exporter import CustomLocalExporter
from nnlo.export.local_generic_exporter import GenericLocalExporter

if __name__ == "__main__":
    driver = JetTrainingDriver()
    exporter = GenericLocalExporter()
    runner = exporter.export(driver)
    runner.run()
