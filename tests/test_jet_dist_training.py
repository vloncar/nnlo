# Script for testing the distributed training using the TFRecords

import os

import tensorflow as tf

from nnlo.data.reading_tfrecords import load_tfrecord

from nnlo.data.adapter import NumpyAdapter
from nnlo.data.dataset import DatasetGenerator
from nnlo.run import TensorflowTrainingRunner
from nnlo.train import TensorflowTrainingDriver
from nnlo.train import SingleNodeStrategy

class CustomLayer(tf.keras.layers.Dense):
    pass

def custom_loss(y_true, y_pred):
    from tensorflow.keras.losses import categorical_crossentropy
    return categorical_crossentropy(y_true, y_pred, from_logits=True)


class JetDataSetGenerator(DatasetGenerator):
    def __init__(self, batch_size) -> None:
        self.train_dataset = load_tfrecord(train_filenames, batch_size)
        self.validation_dataset = load_tfrecord(valid_filenames, batch_size)


    def generate_tf_dataset(self, num_samples, num_records=10):
        return

    def get_train_dataset(self):
        return self.train_dataset

    def get_validation_dataset(self):
        return self.validation_dataset


def model_builder():
    model = tf.keras.Sequential(
        [
            tf.keras.layers.Input(shape=(100, 100, 1)),
            tf.keras.layers.Conv2D(
                kernel_size=(5, 5),
                filters=5,
                strides=(1, 1),
                padding="same",
                data_format="channels_last",
            ),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Activation("relu"),
            tf.keras.layers.MaxPooling2D(pool_size=(5, 5)),
            tf.keras.layers.Dropout(0.25),
            tf.keras.layers.Conv2D(
                kernel_size=(3, 3), filters=3, strides=(1, 1), padding="same"
            ),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Activation("relu"),
            tf.keras.layers.MaxPooling2D(pool_size=(3, 3)),
            tf.keras.layers.Dropout(0.25),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(5, "relu"),
            # CustomLayer(5, "relu"),
            tf.keras.layers.Dense(5, "softmax"),
        ]
    )
    return model


train_filenames = [
    "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_0-of-12.tfrecord",
    "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_1-of-12.tfrecord",
    "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_2-of-12.tfrecord",
    "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_3-of-12.tfrecord",
    "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_4-of-12.tfrecord",
    "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_5-of-12.tfrecord",
    "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_6-of-12.tfrecord",
    "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_7-of-12.tfrecord",
    # "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_8-of-12.tfrecord",
        ]

valid_filenames = [
    "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_9-of-12.tfrecord",
    "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_10-of-12.tfrecord",
    "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_11-of-12.tfrecord",
    "/eos/user/i/iveljano/ml-dl/workspace/nnlo/tests/tfrecords_trial_9/jet_data_12-of-12.tfrecord",
]

data_generator = JetDataSetGenerator(8)
driver = TensorflowTrainingDriver('nnlo_convnet_jet', model_builder, data_generator,
                                loss='categorical_crossentropy', optimizer='adam', dist_trategy=SingleNodeStrategy())
runner = TensorflowTrainingRunner(driver)
runner.run()

